/*
  tdsdltetris
  
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import std.stdio;
import std.format;
import std.random;
import std.conv;
import std.traits;
import std.string;
import derelict.sdl2.sdl;
import derelict.sdl2.ttf;

enum auto EXIT_CALLBACK_LENGTH = 100;
enum auto WINDOW_WIDTH = 800;
enum auto WINDOW_HEIGHT = 600;
enum auto TITLE = "tdsdltetris";
enum auto BLOCK_SIZE = 20;
enum auto MAX_BLOCK  = WINDOW_HEIGHT - BLOCK_SIZE;
immutable SDL_Rect PLAYFIELD_INNER_RECT = {
	1,
	1,
	(10 * BLOCK_SIZE) + 2,
	(22 * BLOCK_SIZE) + 2
};
immutable SDL_Rect PLAYFIELD_OUTER_RECT = {
	PLAYFIELD_INNER_RECT.x - 1,
	PLAYFIELD_INNER_RECT.y - 1,
	PLAYFIELD_INNER_RECT.w + 1,
	PLAYFIELD_INNER_RECT.h + 1,
};
enum Color
{
	NONE   = 0x000000,
	CYAN   = 0x00FFFF,
	YELLOW = 0xFFFF00,
	PURPLE = 0xFF00FF,
	GREEN  = 0x00FF00,
	RED    = 0xFF0000,
	BLUE   = 0x0000FF,
	ORANGE = 0xFF9900
}
enum PLAYFIELD_COLUMNS = 10;
enum PLAYFIELD_ROWS    = 22;
enum ORIENTATION_MAX   =  4;
struct TetrominoTuple
{
	ulong orientations;
	Color color;
}
enum Tetromino
{
	NONE = TetrominoTuple(0x0000000000000000, Color.NONE),
	I    = TetrominoTuple(0x444400F022220F00, Color.CYAN),
	O    = TetrominoTuple(0xCC00CC00CC00CC00, Color.YELLOW),
	T    = TetrominoTuple(0x46404E004C400E40, Color.PURPLE),
	S    = TetrominoTuple(0x46206C008C4006C0, Color.GREEN),
	Z    = TetrominoTuple(0x2640C6004C800C60, Color.RED),
	J    = TetrominoTuple(0x0E2064408E0044C0, Color.BLUE),
	L    = TetrominoTuple(0x2E00C4400E804460, Color.ORANGE)
}

void function()[EXIT_CALLBACK_LENGTH] g_exitCallbacks;
bool          g_quit;
SDL_Window   *g_window;
SDL_Renderer *g_renderer;
Tetromino     g_tetromino;
Tetromino     g_tetromino_next;
uint          g_posX;
uint          g_posY;
uint          g_orientation;
uint          g_score;
uint          g_lines;
uint          g_levels;
Color[PLAYFIELD_ROWS][PLAYFIELD_COLUMNS] g_blocks;
SDL_Texture*[10] g_font;
uint          g_fontW;
uint          g_fontH;
uint          g_nextDown;

void reset ()
{
	g_score = 0;
	g_lines = 0;
	g_levels = 1;
	g_orientation = 0;
	g_tetromino      = [EnumMembers!Tetromino][uniform(1, 8)];
	g_tetromino_next = [EnumMembers!Tetromino][uniform(1, 8)];
	g_posX = 2;
	g_nextDown = SDL_GetTicks() + (1000 - (g_levels * 20));
	uint y = 0;
	while(!isColliding(g_tetromino, g_orientation, g_posX, y - 1))
		--y;
	g_posY = y;
	for (uint column = 0; column < PLAYFIELD_COLUMNS; ++column)
		for (uint row = 0; row < PLAYFIELD_ROWS; ++row)
			g_blocks[column][row] = Color.NONE;
}

bool pushExitCallback (void function() callback)
{
	static int index = 0;	
	assert(callback);
	assert(EXIT_CALLBACK_LENGTH > index, "Exit callbacks array is full.");	
	g_exitCallbacks[index++] = callback;	
	return true;
}

void onExit ()
{
	foreach (callback; g_exitCallbacks)
	{
		if (!callback)
			break;
		callback();
	}
}

void initializeFont()
{
	TTF_Font *font = TTF_OpenFont("Mandrill.ttf", 28);
	immutable SDL_Color color = SDL_Color(255,255,255,255);
	
	if (!font)
		throw new Error(format("ERROR TTF_OpenFont: %s",
				       TTF_GetError()));
	for (int i = 0; i < 10; ++i)
	{
		SDL_Surface* s = TTF_RenderText_Solid(font,
						      toStringz(to!string(i)),
						      color);
		g_fontW = s.w;
		g_fontH = s.h;
		g_font[i] = SDL_CreateTextureFromSurface(g_renderer, s);
		SDL_FreeSurface(s);
	}
	TTF_CloseFont(font);
}

void initialize ()
{	
	DerelictSDL2.load();
	DerelictSDL2ttf.load();
	pushExitCallback(&finalize);
	if (SDL_Init(SDL_INIT_VIDEO))
		throw new Error(format("ERROR SDL_Init: %s", SDL_GetError()));
	g_window = SDL_CreateWindow(TITLE,
				    100,
				    100,
				    WINDOW_WIDTH,
				    WINDOW_HEIGHT,
				    SDL_WINDOW_RESIZABLE);
	if (!g_window)
		throw new Error(format("ERROR SDL_CreateWindow: %s",
				       SDL_GetError()));
	g_renderer = SDL_CreateRenderer(g_window,
					-1,
					SDL_RENDERER_ACCELERATED);
	if (!g_renderer)
		throw new Error(format("ERROR SDL_CreateRenderer: %s",
				       SDL_GetError()));
	SDL_RenderSetLogicalSize(g_renderer, WINDOW_WIDTH, WINDOW_HEIGHT);
	SDL_RenderSetScale(g_renderer, 1.0, 1.0);
	if (TTF_Init())
		throw new Error(format("ERROR TTF_Init: %s", TTF_GetError()));
	initializeFont();
	reset();
}

void finalize ()
{
	TTF_Quit();
	if (g_renderer)
		SDL_DestroyRenderer(g_renderer);
	if (g_window)
		SDL_DestroyWindow(g_window);
	SDL_Quit();
}

void handleEvents ()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
	switch(event.type)
	{
	case SDL_QUIT:
		g_quit = true;
		break;
	case SDL_WINDOWEVENT:
		if (SDL_WINDOWEVENT_RESIZED == event.window.event ||
		    SDL_WINDOWEVENT_SIZE_CHANGED == event.window.event)
		{
			SDL_RenderSetScale(
				g_renderer,
				cast(float)event.window.data1 / WINDOW_WIDTH,
				cast(float)event.window.data2 / WINDOW_HEIGHT);
		}
		break;
	case SDL_KEYDOWN:
		switch (event.key.keysym.sym)
		{
		case SDLK_UP:
			doOrientation();
			break;
		case SDLK_LEFT:
			doMove(-1, 0);
			break;
		case SDLK_RIGHT:
			doMove(1, 0);
			break;
		case SDLK_DOWN:
			while (doMove(0, 1)) {}
			next();
			break;
		case SDLK_r:
			reset();
			break;
		default:
		}
		break;
	default:
	}
	}
}

void renderPlayfield ()
{
	// Border
	SDL_SetRenderDrawColor(g_renderer, 255, 255, 255, 255);
	SDL_RenderDrawRect(g_renderer, &PLAYFIELD_OUTER_RECT);
	// Blocks
	for (uint column = 0; column < PLAYFIELD_COLUMNS; ++column)
	{
		for (uint row = 0; row < PLAYFIELD_ROWS; ++ row)
		{
			//g_blocks[column][row] = [EnumMembers!Color][uniform(0, 8)];
			renderBlock(1 + (column * BLOCK_SIZE),
				    1 + (row * BLOCK_SIZE),
				    g_blocks[column][row]);
		}
	}
}

bool isRowFull (uint row)
{
	for (uint column = 0; column < PLAYFIELD_COLUMNS; ++column)
	{
		if (!g_blocks[column][row])
			return false;
	}
	return true;
}

void renderBlock (uint x, uint y, Color color)
{
	renderBlock(x,
		    y,
		    (color >> 16) & 0xFF,
		    (color >>  8) & 0xFF,
		    color & 0xFF);
}

void renderBlock (uint x, uint y, ubyte r, ubyte g, ubyte b)
{
	immutable SDL_Rect outerRect = {x, y, BLOCK_SIZE, BLOCK_SIZE};
	immutable SDL_Rect innerRect = {
		x + 1,
		y + 1,
		BLOCK_SIZE - 2,
		BLOCK_SIZE - 2
	};
	SDL_SetRenderDrawColor(g_renderer, 0, 0, 0, 255);
	SDL_RenderDrawRect(g_renderer, &outerRect);
	SDL_SetRenderDrawColor(g_renderer, r, g, b, 255);
	SDL_RenderFillRect(g_renderer, &innerRect);
}

void renderTetromino (Tetromino t, uint o, uint pX, uint pY)
{
	for (uint x = 0; x < 4; ++x)
	{
		for (uint y = 0; y < 4; ++y)
		{
			if (!tetrominoSolidAt(t, o, x, y))
				continue;
			immutable uint pixelPosX = 1 + ((pX + x) * BLOCK_SIZE);
			immutable uint pixelPosY = 1 + ((pY + y) * BLOCK_SIZE);
			renderBlock(pixelPosX, pixelPosY, t.color);
		}
	}
}

bool tetrominoSolidAt (Tetromino t, uint o, uint x, uint y)
{
	return (t.orientations >> (o * 16 + (4 * y + x))) & 1 ? true : false;
}

bool isColliding (Tetromino t, uint o, uint pX, uint pY)
{
	for (uint x = 0; x < 4; ++x)
	{
		for (uint y = 0; y < 4; ++y)
		{
			immutable uint aX = x + pX;
			immutable uint aY = y + pY;
			if (!tetrominoSolidAt(t, o, x, y))
				continue;
			if (   aX >= PLAYFIELD_COLUMNS
			    || aY >= PLAYFIELD_ROWS
			    || aY < 0
			    || aX < 0
			    || g_blocks[aX][aY])
				return true;
		}
	}
	return false;
}

void doOrientation ()
{
	immutable uint orientation = (g_orientation + 1) & 3;
	if (!isColliding(g_tetromino, orientation, g_posX, g_posY))
		g_orientation = orientation;
}

bool doMove (uint oX, uint oY)
{
	immutable uint nX = g_posX + oX;
	immutable uint nY = g_posY + oY;
	if (isColliding(g_tetromino, g_orientation, nX, nY))
		return false;
	g_posX = nX;
	g_posY = nY;
	return true;
}

void merge ()
{
	for (uint x = 0; x < 4; ++x)
	{
		for (uint y = 0; y < 4; ++y)
		{
			if (!tetrominoSolidAt(g_tetromino, g_orientation, x, y))
				continue;
			g_blocks[g_posX + x][g_posY + y] = g_tetromino.color;
		}
	}
}

void renderNumber(uint x, uint y, int n)
{
	const SDL_Rect s = {0, 0, g_fontW, g_fontH};
	SDL_Rect d = {x, y, g_fontW, g_fontH};
	for (uint i = 10; i >= 0; --i)
	{
	       	d.x = x + (g_fontW * i);
		SDL_RenderCopy(g_renderer, g_font[n%10], &s, &d);
		n /= 10;
		if (!n)
			break;		
	}
}

void render()
{
	SDL_SetRenderDrawColor(g_renderer, 0, 0, 0, 255);
	SDL_RenderClear(g_renderer);
	renderPlayfield();
	renderTetromino(g_tetromino, g_orientation, g_posX, g_posY);
	renderTetromino(g_tetromino_next, 0, 12, 2);
	renderNumber(250, 200, g_score);
	renderNumber(250, 250, g_lines);
	renderNumber(250, 300, g_levels);
	SDL_RenderPresent(g_renderer);
}

void collapse (uint row)
{
	for (uint r = row; r > 0; --r)
		for (uint column = 0; column < PLAYFIELD_COLUMNS; ++column)
			g_blocks[column][r] = g_blocks[column][r - 1];
	for (uint column = 0; column < PLAYFIELD_COLUMNS; ++column)
		g_blocks[column][0] = Color.NONE;	
}

void next()
{
	merge();
	g_tetromino = g_tetromino_next;
	g_tetromino_next = [EnumMembers!Tetromino][uniform(1, 8)];
	g_posX = 2;
	g_orientation = 0;
	uint y = 0;
	while(!isColliding(g_tetromino, g_orientation, g_posX, y - 1))
		--y;
	g_posY = y;
	uint lines = 0;
	for (uint row = 0; row < PLAYFIELD_ROWS; ++row)
		if (isRowFull(row))
		{
			collapse(row);
			++lines;				
		}
	if (lines)
	{
		g_lines += lines;
		g_levels = 1 + (lines / 100);
		g_score += ((1 << lines) * 1000 * g_levels);
	}
}

void update()
{
	uint ticks = SDL_GetTicks();	
	if (ticks >= g_nextDown)
	{
		if (!doMove(0, 1))
			next();
		g_nextDown = ticks + (1000 - (g_levels * 20));
	}
}

void main()
{
	g_tetromino = [EnumMembers!Tetromino][uniform(1, 8)];
	scope(exit) onExit();
	initialize();
	while (true)
	{
		handleEvents();
		if (g_quit)
			break;
	        render();
		update();
	}     
}
